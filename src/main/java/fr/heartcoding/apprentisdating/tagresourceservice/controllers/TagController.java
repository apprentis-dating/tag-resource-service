package fr.heartcoding.apprentisdating.tagresourceservice.controllers;

import fr.heartcoding.apprentisdating.tagresourceservice.models.Tag;
import fr.heartcoding.apprentisdating.tagresourceservice.services.TagService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@RestController
@RequestMapping()
public class TagController {

    private final TagService tagService;

    public TagController(final TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    @ApiOperation(value = "List all tags of the application", notes = "Return the full list of tag inside application database")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request has succeed, returns the list")
    })
    public List<Tag> list() {
        return this.tagService.list();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @ApiOperation(value = "Add a new tag to the application", notes = "Add a tag by name to the application database")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Request has succeed, model saved in database")
    })
    public Tag create(
            @ApiParam(name = "tag", type = "Tag", value = "Tag to add", required = true)
            @RequestBody Tag tag
    ) {
        return this.tagService.create(tag);
    }

    @GetMapping(value = "{id}")
    @ResponseBody
    @ApiOperation(value = "Get a specific tag", notes = "Find and return a specific tag by its ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request has succeed, tag returned"),
            @ApiResponse(code = 404, message = "Request has failed, missing tag")
    })
    public Tag read(
            @ApiParam(name = "id", type = "String", value = "ID of the tag (UUID)", required = true)
            @PathVariable(name = "id") String id
    ) {
        return this.tagService.read(UUID.fromString(id));
    }

    @PatchMapping(value = "{id}")
    @ResponseBody
    @ApiOperation(value = "Update a specific tag content", notes = "Find and update the contant of a specific tag")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request has succeed, patched tag returned"),
            @ApiResponse(code = 404, message = "Request has failed, missing tag")
    })
    public Tag update(
            @ApiParam(name = "id", type = "String", value = "ID of the tag (UUID)", required = true)
            @PathVariable(name = "id") String id,
            @ApiParam(name = "tag", type = "Tag", value = "Tag to add", required = true)
            @RequestBody Tag tag
    ) {
        return this.tagService.update(tag, UUID.fromString(id));
    }

    @DeleteMapping(value = "{id}")
    @ResponseBody
    @ApiOperation(value = "Delete a specific tag", notes = "Find and remove a specific tag by its ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request has succeed, deleted tag returned"),
            @ApiResponse(code = 404, message = "Request has failed, missing tag")
    })
    public Tag delete(
            @ApiParam(name = "id", type = "String", value = "ID of the tag (UUID)", required = true)
            @PathVariable(name = "id") String id
    ) {
        return this.tagService.delete(UUID.fromString(id));
    }
}
