package fr.heartcoding.apprentisdating.tagresourceservice.dao;

import fr.heartcoding.apprentisdating.tagresourceservice.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface TagDao extends JpaRepository<Tag, UUID> {
}
