package fr.heartcoding.apprentisdating.tagresourceservice.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Table(name = "AD_TRS_TAG")
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "AD_TRS_ID", updatable = false, nullable = false)
    private UUID id;

    @Column(name="AD_TRS_NAME", nullable = false)
    private String name;

}
