package fr.heartcoding.apprentisdating.tagresourceservice.configurations;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration global de l'application.
 * ErrorMvcAutoConfiguration permet de désactiver le page WhiteLabel afin d'avoir des erreurs
 * via les codes de retour des réponse HTTP à la place.
 */
@Configuration
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class GlobalConfiguration {
}
