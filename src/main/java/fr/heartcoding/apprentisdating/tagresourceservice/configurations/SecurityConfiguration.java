package fr.heartcoding.apprentisdating.tagresourceservice.configurations;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ConditionalOnProperty(name = "keycloak.enabled", havingValue = "false", matchIfMissing = true)
@Order(1)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity security) {
        try {
            // Par défaut, l'authentification x509 est désactivée
            security.httpBasic().and().csrf().disable()
            /*.authorizeRequests().anyRequest()
			.authenticated().and().x509()*/;
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}

