package fr.heartcoding.apprentisdating.tagresourceservice.configurations;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.MethodNotAllowedException;

import java.util.NoSuchElementException;

/**
 * Gestion des erreyrs qui peuvent survenir au cours de l'execution de code dans les contoleurs
 * (y compris les services).
 * Dès qu'une exception listée est émise alors cette classe la catch et renvoie
 * une réponse HTTP avec une erreur et un message aqéquate.
 */
@ControllerAdvice(value = "fr.heartcoding.apprentisdating.tagresourceservice.controllers")
public class ExceptionsHandler {

    /**
     * Transforme une exception "pas d'élément trouvé" en erreur 404
     * @param nsee Objet de l'exception
     * @return ResponseEntity<String>
     */
    @ExceptionHandler(value = NoSuchElementException.class)
    public ResponseEntity<ResponseMessage> handleNoSuchElementException(
            final NoSuchElementException nsee
    ) {
        return ResponseMessage.build("Resource not found", nsee.toString(), HttpStatus.NOT_FOUND);
    }

    /**
     * Transforme une exception "objet null" en erreur 500
     * @param npe Objet de l'exception
     * @return ResponseEntity<String>
     */
    @ExceptionHandler(value = NullPointerException.class)
    public ResponseEntity<ResponseMessage> handleNullPointerException(
            final NullPointerException npe
    ) {
        return ResponseMessage.build("Internal Server Error", "An error occurred that the server can't handle [" + npe.getMessage() + "]", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Transforme une exception "mauvaise méthode" en erreur 415
     * @param mnae Objet de l'exception
     * @return ResponseEntity<String>
     */
    @ExceptionHandler(value = MethodNotAllowedException.class)
    public ResponseEntity<ResponseMessage> handleMethodNotAllowedException(
            final MethodNotAllowedException mnae
    ) {
        String message = "[" + mnae.getHttpMethod() + "] not permitted";
        return ResponseMessage.build("HTTP Method not permitted", message, HttpStatus.METHOD_NOT_ALLOWED);
    }

    /**
     * Transforme une exception "Argument illégal" en erreur 400
     * @param iae Objet de l'exception
     * @return ResponseEntity<String>
     */
    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<ResponseMessage> handleIllegalArgumentException(
            final IllegalArgumentException iae
    ) {
        return ResponseMessage.build("Internal Server Error", "An error occurred that the server can't handle [" + iae.getMessage() + "]", HttpStatus.NOT_FOUND);
    }

    /**
     * Transforme une exception "État illégal" en erreur 400
     * @param ise Objet de l'exception
     * @return ResponseEntity<String>
     */
    @ExceptionHandler(value = IllegalStateException.class)
    public ResponseEntity<ResponseMessage> handleIllegalStateException(
            final IllegalStateException ise
    ) {
        return ResponseMessage.build("Internal Server Error", "An error occurred that the server can't handle [" + ise.getMessage() + "]", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
