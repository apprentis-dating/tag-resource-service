package fr.heartcoding.apprentisdating.tagresourceservice.configurations;

import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.preauth.x509.X509AuthenticationFilter;
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
@ConditionalOnProperty(name = "keycloak.enabled", havingValue = "true", matchIfMissing = true)
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class,
        excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern =
                "org.keycloak.adapters.springsecurity.management.HttpSessionManager"))
@EnableWebSecurity
@Order(2)
public class KeycloakSecurityConfiguration extends KeycloakWebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        // Bearer-only application
        return new NullAuthenticatedSessionStrategy();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);

        http
                // disable csrf because of API mode // FIXME
                .csrf().disable()

                .sessionManagement()
                // Utilisation des bens de session constuits par ailleurs
                .sessionAuthenticationStrategy(sessionAuthenticationStrategy())
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                // Filtres de sécu
                .and().addFilterBefore(keycloakPreAuthActionsFilter(), LogoutFilter.class)
                .addFilterBefore(keycloakAuthenticationProcessingFilter(), X509AuthenticationFilter.class)
                .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())

                // Endpoint de déconnexion

                .and().logout().addLogoutHandler(keycloakLogoutHandler())
                .logoutUrl("/logout").logoutSuccessHandler(
                // logout handler for API
                (HttpServletRequest req, HttpServletResponse resp, Authentication auth) ->
                        resp.setStatus(HttpServletResponse.SC_OK)
        )
                .and()
                // manage routes securisation here // FIXME
                .authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll()

                // Ouverture de swagger */
                .antMatchers("/v2/api-docs").permitAll()

                // Ouverture de l'actuator
                .antMatchers("/actuator").permitAll().antMatchers("/actuator/**").permitAll()

                // Protection de l'ensemble des endpoints
                .antMatchers(HttpMethod.GET, "/tags").authenticated()
                .antMatchers(HttpMethod.GET, "/tags/**").authenticated()
                .antMatchers(HttpMethod.POST, "/tags").hasRole("admin")
                .antMatchers(HttpMethod.PATCH, "/tags/**").hasRole("admin")
                .antMatchers(HttpMethod.DELETE, "/tags/**").hasRole("admin")
                .anyRequest().denyAll();

        http.cors().disable();
    }
}

