package fr.heartcoding.apprentisdating.tagresourceservice.services;

import fr.heartcoding.apprentisdating.tagresourceservice.dao.TagDao;
import fr.heartcoding.apprentisdating.tagresourceservice.models.Tag;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
public class TagService {

    private final TagDao tagDao;

    public TagService(final TagDao tagDao) {
        this.tagDao = tagDao;
    }

    public List<Tag> list() {
        return this.tagDao.findAll();
    }

    public Tag create(final Tag tag) {
        return this.tagDao.save(tag);
    }

    public Tag read(final UUID id) {
        return this.tagDao.getOne(id);
    }

    public Tag update(final Tag tag, final UUID tagId) {
        Optional<Tag> optionalTag = this.tagDao.findById(tagId);

        if (optionalTag.isEmpty()) {
            throw new NoSuchElementException();
        }

        if (!tag.getName().isEmpty()) {
            optionalTag.get().setName(tag.getName());
        }

        return this.tagDao.save(optionalTag.get());
    }

    public Tag delete(final UUID id) {
        Tag tag = this.read(id);

        this.tagDao.delete(tag);

        return tag;
    }
}
