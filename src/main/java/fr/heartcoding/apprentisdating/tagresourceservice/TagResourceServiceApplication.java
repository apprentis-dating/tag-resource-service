package fr.heartcoding.apprentisdating.tagresourceservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TagResourceServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TagResourceServiceApplication.class, args);
    }

}
