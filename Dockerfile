FROM maven:3.6.3-openjdk-11 AS MAVEN_BUILD

COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn package -DskipTests

FROM openjdk:11-jre

ARG JAR_FILE=target/tag-resource-service*.jar

RUN ls /tmp
COPY --from=MAVEN_BUILD /tmp/${JAR_FILE} /usr/local/bin/app.jar
COPY --from=MAVEN_BUILD /tmp/${APM_FILE} /usr/local/bin/

WORKDIR /usr/local/bin/

ENV SERVER_PORT 5000

EXPOSE 5000

ENTRYPOINT [ "sh", "-c", "java -jar $VM_OPTIONS app.jar"]
